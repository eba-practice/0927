$(function () {

    /* 上部からリンク用アコーディオンが出現 */
    var Accordion = function (el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        var dropdownlink = this.el.find('.dropdownlink');
        dropdownlink.on('click',
            { el: this.el, multiple: this.multiple },
            this.dropdown);
    };

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            //show only one menu at the same time
            $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
        }
    }
    var accordion = new Accordion($('.accordion-menu'), false);


    /*　スクロールされるとロゴが出現　*/
    // $(window).scroll(function () {
    //     if ($(this).scrollTop() > 100) {
    //         $('div#top-fixed').css('background-image', 'url(./img/sp/titlelogo.png)');
    //     } else {
    //         $('div#top-fixed').css('background-image', 'none');
    //     }
    // });

    /* スムーズスクロール */
    $('[href^="#"]').click(function () {
        var speed = 800;
        var href = $(this).attr('href');
        var target = $(href == '#' || href == '' ? 'html' : href);
        var position = target.offset().top;
        $('html, body').animate({ scrollTop: position }, speed, 'swing');
        return false;
    });

    /*　「topへ戻る」が下へずらすと消える */
    var topBtn = $('.top');
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    topBtn.click(function () {
        $('body,html').animate(
            {
                scrollTop: 0
            },
            500
        );
        return false;
    });

})

//doctorIntroduction
$(function () {
    var checkWidth = function () {
        var browserWidth = $(window).width();
        // console.log(browserWidth);
        $('.row').each(function () {
            var txt = $(this).html();
            if (browserWidth <= 767) {
                console.log('入った')
                $(this).html(
                    txt.replace(/今村宗嗣/g, '●')
                        .replace(/今村友美/g, '●')
                );
            } else {
                $(this).html(
                    txt.replace(/●/g, '今村宗嗣')
                    // .replace(/●/g, '今村友美')
                );
                console.log($("#pink").text())
                $("#pink1").text('今村友美')
                $("#pink2").text('今村友美')

            }
        });
    }
    $(function () {
        checkWidth();
        $(window).resize(checkWidth);
    });
});

//info
$(function () {
    var checkWidth = function () {
        var browserWidth = $(window).width();
        // console.log(browserWidth);
        if (browserWidth >= 767) {
            $('#info dt p').each(function () {
                var txt = $(this).html();
                console.log(txt)
                $(this).html(
                    txt.replace(/<br>/g, ',')
                );
            });
        } else {
            $('#info dt p').each(function () {
                var txt = $(this).html();
                console.log(txt)
                $(this).html(
                    txt.replace(/,/g, '<br>')
                );
            });
        }
    };

    $(function () {
        checkWidth();
        $(window).resize(checkWidth);
    });

    $(function () {
        $('.js-modal-open').each(function () {
            $(this).on('click', function () {
                var target = $(this).data('target');
                var modal = document.getElementById(target);
                $(modal).fadeIn();
                return false;
            });
        });
        $('.js-modal-close').on('click', function () {
            $('.js-modal').fadeOut();
            return false;
        });
    });
});   
