参考サイト：https://sakura-immr.wixsite.com/kasugacho-clinic

※今は非公開のページとしたいので、全ページのhead内に下記のmetaタグを必ず付けてください。  
「一般的には検索結果に出す必要の無い、または出したくないページがある場合にnoindexタグ」
```    
　<meta name="robots" content="noindex,nofollow,noarchive">  
```

締切：10月11日（金）

### 命名規則

キャメルケース　→　プログラミングでの関数や変数  
スネークケース　→　DBなどで扱う値  
ケバブケース　→　HTMLのclass名  

### fontサイズの指定

参照：https://coliss.com/articles/build-websites/operation/css/font-size-used-responsive-scales.html